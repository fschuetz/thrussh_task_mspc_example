mod infrastructure;

#[macro_use] extern crate log;
extern crate env_logger;
extern crate futures;
extern crate tokio;
extern crate signal_hook;
extern crate serde;
use std::collections::HashMap;

use thrussh::CryptoVec;
use tracing::instrument;
use infrastructure::{Command, ClientId};

struct PlayerInfo {
    player_name: String,
    active_session: (thrussh::ChannelId, thrussh::server::Handle)
}

impl PlayerInfo {
    pub fn new(player_name: String, active_session: (thrussh::ChannelId, thrussh::server::Handle)) -> PlayerInfo {
        PlayerInfo {
            player_name,
            active_session
        }
    }
}

#[instrument]
#[tokio::main]
async fn main() {
    // Set up logging
    env_logger::init();
    console_subscriber::init();

    // Configure the ssh server
    let (sh, config,
        mut sender_data_rx, mut sender_command_rx)
        = infrastructure::ssh_server::init_ssh_server();

    // Spawn World Thread
    tokio::spawn(async move{

        let mut players : HashMap<ClientId, PlayerInfo>= HashMap::new();

        loop {
            tokio::select! {
                Some(command) = sender_command_rx.recv() => {
                    info!("Got handle from command rx");
                    match command {
                        Command::Register(client_id, username, channel_id, mut handle) => {
                            // TODO - remove currently active sessions (if any) if a new one is registered
                            players.insert(client_id, PlayerInfo::new(username, (channel_id, handle.clone())));
                            handle.data(channel_id, CryptoVec::from_slice(
                                "Client registered to World!\r\n".as_ref()))
                                .await.expect("Could not send registration msg.");
                        },
                        Command::Hangup(_) => todo!(),
                    };
                }
                Some(v) = sender_data_rx.recv() => {
                    info!("Got {:?} from data tx of client {}", v.data, v.client_id);
                    match players.get(&v.client_id) {
                        Some(player_info) => {
                            player_info.active_session.1.clone().data(player_info.active_session.0, 
                                CryptoVec::from_slice(v.data.clone().as_ref()))
                                .await.expect("Could not send data message to client.");
                        },
                        None => {error!("Player for client not found.");}
                    };
                    
                }
                else => {
                    error!("Both channels closed");
                }
            }
        } 
    });

    // Start the ssh server and listen for incoming connections
    info!("Spawning ssh server listening at: {}", "0.0.0.0:2222");
    thrussh::server::run(config, "0.0.0.0:2222", sh).await.unwrap();
}


