extern crate thrussh;
extern crate futures;
extern crate tokio;
use crate::futures::FutureExt;
use std::pin::Pin;

use std::sync::Arc;
use thrussh::*;
use thrussh::server::{Auth, Session};
use anyhow;
use tokio::sync::mpsc;
use tokio::sync::mpsc::{Receiver, Sender};

use super::{Command, Data, DataMessage};


#[derive(Clone)]
pub struct Server {
    client_id: usize,
    client_username: Option<String>,
    data_buffer: Data,
    tx_data_channel: Sender<DataMessage>,
    tx_command_channel: Sender<Command>, 
}

impl server::Server for Server {
    type Handler = Self;
    fn new(&mut self, _: Option<std::net::SocketAddr>) -> Self {
        let s = self.clone();
        self.client_id += 1;
        s
    }
}

impl server::Handler for Server {
    type Error = anyhow::Error;
    type FutureAuth = futures::future::Ready<Result<(Self, server::Auth), anyhow::Error>>;
    type FutureUnit = Pin<Box<dyn futures::Future<Output = Result<(Self, Session), anyhow::Error>> + std::marker::Send>>;
    type FutureBool = futures::future::Ready<Result<(Self, Session, bool), anyhow::Error>>;

    fn finished_auth(self, auth: Auth) -> Self::FutureAuth { futures::future::ready(Ok((self, auth))) }
    fn finished_bool(self, b: bool, s: Session) -> Self::FutureBool { futures::future::ready(Ok((self, s, b))) }
    fn finished(self, s: Session) -> Self::FutureUnit {
        Box::pin(futures::future::ready(Ok((self, s))))
    }

    fn auth_password(mut self, user: &str, password: &str) -> Self::FutureAuth {
        if password == "Test" {
            self.client_username = Some(user.to_string());
            futures::future::ready(Ok((self, server::Auth::Accept)))
        } else {
            futures::future::ready(Ok((self, server::Auth::Reject)))
        }
    }

    //noinspection ALL
    fn channel_open_session(self, channel: ChannelId, mut session: Session) -> Self::FutureUnit {
        let handle = session.handle().clone();
        let registration_command = Command::Register(self.client_id, self.client_username.clone().unwrap(), channel, handle);
        async move {
            // Register client with the world - pass the handle to world thread
            //
            // This needs to be done to enable the world thread to send data to the
            // ssh user (eg. a description or a result).
            if let Err(_) = self.tx_command_channel.send(registration_command).await {
                error!("channel_open_session(): receiver dropped");
            } else {
                info!("channel_open_session(): Sent client id and handle to world.")
            };

            // Display a welcome message
            session.data(channel,CryptoVec::from_slice("Welcome.\r\n".as_ref()));
            Ok((self, session))
        }.boxed()
    }


    fn data(mut self, _channel: ChannelId, data: &[u8], session: server::Session) -> Self::FutureUnit { 
              
        //Check if the data contains a CR, which is the indicator that the command
        //should be sent to the world.
        let send_condition = data.as_ref() == "\u{000d}".as_bytes();
        let mut data_to_send = None;
        
        if !send_condition {
            // No need to send data to world. Add what we received to buffer
            self.data_buffer.extend_from_slice(data);
        } else {
            // Need to send data to world. Prepare data to send and purge form buffer
            data_to_send = Some(self.data_buffer.clone());
            self.data_buffer.clear();
        }

        //let data_message = DataMessage::new(self.client_id, data_to_send);
        let tx = self.tx_data_channel.clone();
        async move {
            match data_to_send {
                Some(data) => {
                    let data_message = DataMessage::new(self.client_id, data);
                    if let Err(_) = tx.send(data_message).await { 
                        println!("data(): receiver dropped");
                    };
                },
                None => {}
            }
            /* 
            if send_condition {
                // Send data to world instance on CR 
                if let Err(_) = tx.send(data_message).await { 
                    println!("data(): receiver dropped");
                };
            }
            */
            Ok((self, session))
        }.boxed()
    }
}

pub fn init_ssh_server() -> (Server, Arc<thrussh::server::Config>,
                             Receiver<DataMessage>, Receiver<Command>) {
    // Configure the server
    let mut config = thrussh::server::Config::default();
    config.connection_timeout = Some(std::time::Duration::from_secs(600));
    config.auth_rejection_time = std::time::Duration::from_secs(3);
    config.keys.push(thrussh_keys::key::KeyPair::generate_ed25519().unwrap());
    config.auth_banner = Some("Please log in with password \"Test\"\n");
    let config = Arc::new(config);

    // The data channel: The channel players use to send actions etc....
    let (data_tx, data_rx) = mpsc::channel(1_024);

    // The command channel: The channel used to send requests from the session to the world
    let (command_tx, command_rx) = mpsc::channel(1_024);


    // Create the server
    let sh = Server{
        client_username: None,
        client_id: 0,
        data_buffer: Data::new(),
        tx_data_channel: data_tx.clone(),
        tx_command_channel: command_tx.clone()
    };

    (sh, config, data_rx, command_rx)
}
