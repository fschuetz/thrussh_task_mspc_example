# thrussh_task_mspc_example

An example how to forward input from an ssh session established by thrussh to another tokio task that then sends output to the ssh client.

## Getting started
To demonstrate how to take input from an ssh session and forward it to another thread which then in turn sends something to the ssh client this programm implements a small echo server. You can connect to the server with ssh on localhost, port 222


Run the server example with the following command:
```
RUSTFLAGS="--cfg tokio_unstable" RUST_LOG="debug" RUST_BACKTRACE=1 cargo run
```

The RUSTFLAGS must be set mandatorily as we use tokio-console to allow you investigate the behaviour of the threads. The log level can be set according to your need and you can disable backtracing if you want to.

Connect to the server with the following command:
```
ssh localhost -p 2222
```

It is important to note, that the server generates a new host key on every startup. Therefore you need to delete the server from your known_hosts file of ssh after a server restart. Therefore, a more convenient way to connect is using the following command:
```
ssh -o "UserKnownHostsFile=/dev/null" -o PasswordAuthentication=yes -o PreferredAuthentications=keyboard-interactive,password -o PubkeyAuthentication=no -o StrictHostKeyChecking=no localhost -p 2222
```

This avoids adding the server key to your known_hosts files and sets the authentication method to password.

## Anatomy of the exmaple
This exmple sketches how to build a ssh server using the thrussh crate that receives commands from clients, forwards them to a thread (game world) and allows that world thread to send replies to the clients (players)..

To achieve communication from the ssh server to the world thread I open a command and a data channel as mspc::channel stored in the server struct and the rx side moved into the world thread. I use a command and a data channel as the command channel allows registration of the client and (not in this exmample) other housekeeping messages to be passed and treated specially by the world thread. The data channel is used for the user interaction. In this example the data channel sends all captured text once a cariage return is hit. Note that in this example, we expect the carriage return to be sent by the client as a single character. Theoretically, we could also receive multiple charactes (as the data() function takes an array of u8) and we would need to deal with that. As this however should not happen by just opening a ssh session on your localhost we do not deal with this case for the sake of simplicity of this example. 

To send data to clients of the server, one needs to use handle.data() function. To enable the world thread to send data, I send the ClientId and the handle to the session in the channel_open_session() function - which initiates the ssh server session - over the command channel to the world thread that in turn then uses this handle to send messages through handle.data() (arbitrary text, eg. describing the world) to the ssh user. 

## More information
The example was mainly made as my first aproach using a lock in the data() function and a busy wait loop in the world thread lead to a deadlock. The discussion that led to this solution can be found at the [rust-lang forum](https://users.rust-lang.org/t/deadlock-using-thrussh-to-build-server/70029/8). 
